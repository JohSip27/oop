<?php
require_once('Frog.php');
require_once('animal.php');
require('Ape.php');

$sheep = new Animal("shaun");
echo "Name :$sheep->name<br>"; // "shaun"
echo "Legs : $sheep->legs<br>"; // 4
echo "cold blooded : $sheep->cold_blooded<br>"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())
// index.php


$kodok = new Frog("buduk"); // "hop hop"
echo "<br>Name :$kodok->name<br>"; // "shaun"
echo "Legs : $kodok->legs<br>"; // 4
echo "cold blooded : $kodok->cold_blooded<br>"; // "no"
echo "jump : "; $kodok->jump();

echo "<br><br>";
$sungokong = new Ape("kera sakti");
echo "Name :$sungokong->name<br>"; // "shaun"
echo "Legs : $sungokong->legs<br>"; // 4
echo "cold blooded : $sungokong->cold_blooded<br>"; // "no"
echo "jump : "; $sungokong->yell();
?>